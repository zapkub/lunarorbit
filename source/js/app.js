var app = angular.module('orbitHUD',[]);

app.controller('hudController',['$scope',function($scope){
  $scope.toogleMoonView = function(){
      targetOnMoon();
  }
  $scope.toogleCompare = function(){
    comparePlanets();
  }
}])


var renderer, scene, camera, controls;
var cssRenderer, cssScene, cssCamera;
var planets = {

};
var width = window.innerWidth;
var height = window.innerHeight;
var HUD = document.createElement('div');
HUD.id = 'hud';
document.body.appendChild(HUD);

function init() {
  renderer = new THREE.WebGLRenderer({
    alpha: true
  });
  renderer.setClearColor(0x000000);
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMapEnabled = true;
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 4000);
  camera.position.z = 20;
  scene.add(camera);

  var ambientLight = new THREE.AmbientLight(0x555555);
  scene.add(ambientLight);
  var lights = [];
  lights[0] = new THREE.SpotLight(0xffffff);
  lights[0].castShadow = true;
  lights[0].shadowMapWidth = 1024;
  lights[0].shadowMapHeight = 1024;
  lights[0].position.set(0, 0, 200);

  scene.add(lights[0]);

  renderer.domElement.style.position = "absolute";
  renderer.domElement.style['z-index'] = '-1';
  renderer.domElement.style.top = 0;
  renderer.domElement.style.left = 0;
  document.body.appendChild(renderer.domElement);

  //CSSRenderer

  cssRenderer = new THREE.CSS3DRenderer();
  cssRenderer.setSize(window.innerWidth, window.innerHeight);
  cssScene = new THREE.Scene();
  //cssCamera = new THREE.OrthographicCamera(-width/2, width/2, height/2, -height/2, 0, 30 );
  cssCamera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 1, 10)
  cssCamera.position.z = 140;
  camera.position.y = 3;
  //document.body.appendChild(cssRenderer.domElement);
  cssRenderer.domElement.style.background = "transparent"
  cssScene.add(cssCamera);


  initControls();
  createPlanet();
  createOrbitPath();
  animate();
}


function initControls() {
  controls = new THREE.OrbitControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.1;
  controls.minPolarAngle = 45 * Math.PI / 180;
  controls.maxPolarAngle = 135 * Math.PI / 180;
  controls.userRotateSpeed = 0.5;
  // controls.minAzimuthAngle = -45 * Math.PI / 180;
  // controls.maxAzimuthAngle = 45 * Math.PI / 180;
  controls.minDistance = 90;
  controls.maxDistance = 1000;
  controls.addEventListener('change', render);
  controls.enabled = true;
  controls.rotateSpeed = 0.05;
}
var scale = 1 / 500000;

var earthDensity = 5.51 * 1e+3; // kg/m3
var earthRadius = 6371*1000 * scale; // metre
var earthMass = earthDensity * (4/3) * (Math.PI * Math.pow(earthRadius,3));
var earthRotationAngularVelocity = 7.2921159 * 1e-5;

var moonRadius = 1737*1000 * scale; // metre
var moonDensity = 3.34* 1e+3; // kg/m3
var moonMass = moonDensity * (4/3) * (Math.PI * Math.pow(moonRadius,3));

var distanceFromEarth = (363104*1000 * scale) + (earthRadius); // metre

//Gravitational Force Constant
var G = 6.67300 * 1e-11;
//
var velocity = Math.sqrt((G * earthMass)/distanceFromEarth)
var acceleration = Math.pow(velocity,2)/distanceFromEarth;
var angularVelocity = Math.sqrt(acceleration*distanceFromEarth);
console.log(angularVelocity);

var earthLabelObject, moonLabelObject;
var earthLabel,moonLabel;
var moonMesh ;

function createPlanet() {

  var textureLoader  = new THREE.TextureLoader();

  var earthGeometry = new THREE.SphereGeometry(earthRadius, 32, 32);
  var earthMaterial = new THREE.MeshLambertMaterial({
    map: textureLoader.load('images/earthTexture.jpg')
  });
  var earthMesh = new THREE.Mesh(earthGeometry, earthMaterial);
  earthMesh.rotation.y = 180 * Math.PI / 180;
  planets.earth = earthMesh;

  earthLabel = document.createElement('div');
  earthLabel.className = "label";
  earthLabel.innerHTML = "<h1 class='ui blue ribbon label'>Earth</h1><h3>PlanetEarth</h3> <p> /ˈɜrθ/ is the third planet from the Sun, the densest planet in the Solar System, the largest of the Solar System's four terrestrial planets, and the only astronomical object known to harbor life. <a href='https://en.wikipedia.org/wiki/Earth'>Wikipedia</a></p>";

  moonLabel = document.createElement('div');
  moonLabel.className = "label";
  moonLabel.innerHTML = "<h1 class='ui yellow ribbon label'>Lunar/Moon</h1><h3> Natural Satellite</h3> <p>The Moon is Earth's only natural satellite. It is one of the largest natural satellites in the Solar System, and, among planetary satellites, the largest relative to the size of the planet it orbits. <a href='https://en.wikipedia.org/wiki/Moon'>Wikipedia</a></p>"
  hud.appendChild(moonLabel);
  hud.appendChild(earthLabel);

  scene.add(earthMesh);
  var moonGeometry = new THREE.SphereGeometry(moonRadius, 32, 32);
  var moonMaterial = new THREE.MeshLambertMaterial({map:textureLoader.load('images/moon.jpg')});




  moonMesh = new THREE.Mesh(moonGeometry, moonMaterial);
  planets.moon = moonMesh;

  moonMesh.position.x = distanceFromEarth;
  scene.add(moonMesh);

}
var clock = new THREE.Clock();

function animate() {
  requestAnimationFrame(animate);
  render();
  controls.update();
}
var moonOrbitPath;
var createOrbitPath = function() {
  if(!moonOrbitPath){
    var material = new THREE.LineDashedMaterial( { color: 0xffaa00, dashSize: 3, gapSize: 1, linewidth: 1 } );
    var radius = distanceFromEarth;
    var segments = 108; //<-- Increase or decrease for more resolution I guess
    var circleGeometry = new THREE.CircleGeometry(radius, segments);
    circleGeometry.vertices.shift();
    moonOrbitPath = new THREE.LineSegments(circleGeometry, material);
    moonOrbitPath.rotation.x = 90 * Math.PI/180;
  }

  scene.add(moonOrbitPath);
}
var removeOrbitPath = function(){
  scene.remove(moonOrbitPath);
}


var totalTime = 0;
var speedTime = 360;
var distanceRadius = 30;
var render = function() {
  var delta = clock.getDelta();
  planets.earth.rotation.y += delta * 360 * (earthRotationAngularVelocity );// 
  var elapsed = clock.getElapsedTime();
  var moon = planets.moon;

    moon.position.z = -Math.sin(elapsed * (angularVelocity * 360) * Math.PI / 180) * distanceRadius;
    moon.position.x = -Math.cos(elapsed * (angularVelocity * 360) * Math.PI / 180) * distanceRadius;

  moon.rotation.y += delta * 10 * Math.PI / 180;

  var postionEarthIn2D = toScreenXY(planets.earth, camera);
  earthLabel.style.left = (postionEarthIn2D.x) + "px";
  earthLabel.style.top = (postionEarthIn2D.y) + "px";

  var positionMoonIn2d = toScreenXY(planets.moon,camera);
  moonLabel.style.left = positionMoonIn2d.x + "px";
  moonLabel.style.top = positionMoonIn2d.y + "px";

  if(isTargetOnMoon)
  {
     controls.target.set(moonMesh.position.x,moonMesh.position.y,moonMesh.position.z) ;

  }else{
      controls.target.set(planets.earth.position.x,planets.earth.position.y,planets.earth.position.z)
  }

  camera.updateProjectionMatrix();
  renderer.render(scene, camera);
  cssRenderer.render(cssScene, cssCamera);


}
window.addEventListener('resize', onWindowResize, false);

function onWindowResize() {
  width = window.innerWidth;
  height = window.innerHeight;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

init();

var isTargetOnMoon = false;
var targetOnMoon = function(){

  if(isTargetOnMoon){
    TweenLite.to(camera.position,0.5,{z:planets.moon.position.z,x:planets.moon.position.x,y:planets.moon.position.y})
  }else{
    TweenLite.to(camera.position,0.5,{z:planets.earth.position.z,x:planets.earth.position.x,y:planets.earth.position.y+20})
  }
  isTargetOnMoon = !isTargetOnMoon;
}
var isCompare = true;
var comparePlanets = function(){

    if(!isCompare){
      TweenLite.to(window,1.5,{distanceRadius:30});

    }else{
      TweenLite.to(window,1.5,{distanceRadius:distanceFromEarth})
    }
      isCompare = !isCompare;
}

function toScreenXY(obj, camera) {
  var vector = obj.position.clone();
  var canvas = renderer.domElement;
  vector.project(camera);
  vector.x = Math.round((vector.x + 1) * canvas.width / 2),
  vector.y = Math.round((-vector.y + 1) * canvas.height / 2);
  vector.z = 0;
  return {
    x: vector.x / window.devicePixelRatio,
    y: vector.y / window.devicePixelRatio
  };

}
