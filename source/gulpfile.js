var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var useref = require('gulp-useref');

gulp.task('serve',['sass'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("*.html").on('change', browserSync.reload);
    gulp.watch("js/*.js").on('change', browserSync.reload);
    gulp.watch("scss/*.scss",['sass']);
});

gulp.task('sass',function(){
  return gulp.src('./scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({ browsers: ['last 8 versions'] }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});
gulp.task('build',['min-js','copy-css','copy-image']);

gulp.task('min-js',function(){
  return gulp.src('./index.html')
          .pipe(useref())
          .pipe(gulp.dest('dist'));
})
gulp.task('copy-css',function(){
  return gulp.src('./css/**')
        .pipe(gulp.dest('dist/css'));
})
gulp.task('copy-image',function(){
  return gulp.src('./images/**')
        .pipe(gulp.dest('dist/images'));
})
gulp.task('default', ['serve']);
